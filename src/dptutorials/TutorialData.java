package dptutorials;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Data holder class for a design pattern tutorial.
 * @author Matti Martikainen
 */
class TutorialData {
    private final String patternName;
    private final ArrayList<String> tags;
    private final String shortDescription;
    private final String htmlPath;

    public TutorialData(String name, ArrayList<String> tags, String shortDescription, String htmlPath) {
        this.patternName = name;
        this.tags = tags;
        this.shortDescription = shortDescription;
        this.htmlPath = htmlPath;
    }

    String getPatternName() {
        return patternName;
    }

    ArrayList<String> getTags() {
        return tags;
    }

    String getHtmlPath() {
        return htmlPath;
    }

    String getShortDescription() {
        return shortDescription;
    }

    // Check if a tag exists in the list.
    boolean hasTag(String tag) {
        if (tag.equalsIgnoreCase(patternName)) {
            return true;
        }

        for (String t : tags) {
            if (tag.equalsIgnoreCase(t)) {
                return true;
            }
        }
        return false;
    }

    // Converts an ArrayList of tags to a single comma-separated string.
    static String formatTagsAsString(ArrayList<String> tags) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tags.size(); i++) {
            String s = tags.get(i);
            sb.append(s);
            if (i + 1 < tags.size())
                sb.append(", ");
        }
        return sb.toString();
    }
}
